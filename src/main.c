#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "main.h"
#include "device.h"
#include "sleep_ex_api.h"

#include "wlan_intf.h"
#include <lwip/sockets.h>

#include "sensor_util.h"
#include "nfc_util.h"
#include "wifi_helper.h"
#include "acc_adxl345.h"
#include "lcd_i2c.h"
#include "LSM6DS3.h"

extern osThreadId nfc_tid;
extern void wlan_netowrk(void);
extern void console_init(void);

extern const char *ip;
extern unsigned short port;
unsigned short useWifi = 1;

QueueHandle_t qh = 0;
i2c_t i2c_obj;

// TH02 is a pain in the ass.
// So give it an independent i2c obj.
i2c_t i2c_obj_th02;
// unsigned short goToSleep = 0;


void sensorTask() {
    /**
     *  Power Switch Control
     */
    initSensorPowerSwitch();
    DiagPrintf("Powering up sensors\n");
    powerUpSensors();
    vTaskDelay(1000/portTICK_PERIOD_MS);
    DiagPrintf("After powering up sensors\n");

    sensor_buffer buf;
    unsigned short counter = 0;
    i2c_init(&i2c_obj, ACC_I2C_MTR_SDA, ACC_I2C_MTR_SCL);
    i2c_frequency(&i2c_obj, MBED_I2C_BUS_CLK);

    /**
     *  These three sensors share the same I2C object.
     */
    LSM9DS1_init(&i2c_obj, IMU_MODE_I2C, LSM9DS1_AG_ADDR(1), LSM9DS1_M_ADDR(1));
    LSM9DS1_initAccel();
    acc_adxl345_init(&i2c_obj);
    init_lcd(&i2c_obj, LCD_ADDRESS, 16, 2);

    /**
     *  TH02 humidity and temp sensor.
     */
    i2c_init(&i2c_obj_th02, MBED_I2C_MTR_SDA ,MBED_I2C_MTR_SCL);
    i2c_frequency(&i2c_obj_th02, MBED_I2C_BUS_CLK);
    TH02_Init(&i2c_obj_th02);

    /**
     *  Init analog port. Usually this is used to communicate
     *  with loudness sensor.
     */
    initAnalog(1);

    lcd_backlight();
    lcd_print("hello!");
    float accX, accY, accZ;
    float accGX, accGY, accGZ;
    float temp, humidity;
    int32_t loudness;
    const TickType_t xDelay = 5000/portTICK_PERIOD_MS;
    unsigned short powerStatus = 1;

    DiagPrintf("Sensor task loop begin\n");
    while(1) {
        DiagPrintf("loop\n");
        if (!powerStatus) {
          powerUpSensors();
          powerStatus = 1;
        }
        // Collect data twice and sleep for a minute
        if (counter >= 3) {
          counter = 0;
          powerStatus = 0;
          printf("gotosleep\n");
          powerDownSensors();
          vTaskDelay(xDelay);
          continue;
          sleep_ex(SLEEP_WAKEUP_BY_STIMER, 20000);
          deepsleep_ex(DSLEEP_WAKEUP_BY_TIMER, 3000);
        }
        counter++;

        LSM9DS1_readAccel(&buf.accGX, &buf.accGY, &buf.accGZ);

        acc_adxl345_read_acc(&accX, &accY, &accZ);
        buf.accX = accX;
        buf.accY = accY;
        buf.accZ = accZ;

        temp = TH02_ReadTemperature();
        humidity = TH02_ReadHumidity();
        buf.temp = temp;
        buf.humidity = humidity;

        loudness = readAnalog(1);
        buf.loudness = loudness;

        // rtl_printf("Sending: accX %3.2f, accY %3.2f, accZ %3.2f\n", (((float) settings.accel.scale)/32768.0)*buf.accGX,
        // (((float) settings.accel.scale)/32768.0)*buf.accGY,
        // (((float) settings.accel.scale)/32768.0)*buf.accGZ);
        // rtl_printf("Sending: accX %" PRIu16 "\n", buf.accGX);
        // rtl_printf("Sending: temp %3.2f, humidity %3.2f\n", temp, humidity);
        // rtl_printf("Sending: loudness %u\n", loudness);

        /**
         *  Show sensor collected data on the LCD panel.
         */
        char myData[16];
        // sprintf(myData, "accX %3.2f Y %3.2f", accX, accY);
        sprintf(myData, "temp %3.2f hum %3.2f", temp, humidity);
        lcd_clear();
        lcd_print(myData);
        lcd_setCursor(0, 1);
        // sprintf(myData, "Z %3.2f T %3.2f", accZ, temp);
        sprintf(myData, "loudness %d", loudness);
        lcd_print(myData);

        vTaskDelay(xDelay);

        if(!xQueueSend(qh, &buf, 1000)) {
          puts("Failed to send item to queue within 500ms");
        }
    }
}

void clntTask() {
  sensor_buffer buf;
  int cnt = 0;

  while(1) {
    if(!xQueueReceive(qh, &buf, 1000)) {
        // puts("Nothing to receive item within 1000 ms");
    } else {
      // rtl_printf("Received: accX %3.2f, accY %3.2f, accZ %3.2f\n", buf.accX, buf.accY, buf.accZ);
      rtl_printf("Received: accX %d, accY %d, accZ %d\n", buf.accGX, buf.accGY, buf.accGZ);
      rtl_printf("Received: temp %3.2f, humidity %3.2f\n", buf.temp, buf.humidity);
      rtl_printf("Received: loudness %u\n", buf.loudness);

      if (rltk_wlan_running(WLAN0_IDX) && useWifi) {
        /**
         *  Send data to our local server.
         */
        uploadSensorData(&buf, sizeof(sensor_buffer));

        /**
         *  Send data to firebase (but it never worked XD).
         */
        sendToFirebase(&buf);

        /**
         *  Obtain precise timing info from NTP server.
         */
        if (cnt < 1) {
          sendNTPpacket();
          cnt++;
        } else cnt = 0;
      }
    }
  } // End of while(1)
}



/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void) {
	if ( rtl_cryptoEngine_init() != 0 ) {
		DiagPrintf("crypto engine init failed\r\n");
	}
  //
	// /* Initialize log uart and at command service */
	// console_init();
  //

	/* wlan intialization */
#if defined(CONFIG_WIFI_NORMAL) && defined(CONFIG_NETWORK)
  if (useWifi) {
  	wlan_network();
  }
#endif

qh = xQueueCreate(1, sizeof(sensor_buffer));
  if (useWifi) {
    xTaskCreate(TryAssociate, "wifi", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
  }
  xTaskCreate(sensorTask, "sensor task", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
  xTaskCreate(clntTask, "clnt task", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);

  /**
   *  NFC Task
   */
  osThreadDef(nfc_task, osPriorityRealtime, 1, 1024);
  nfc_tid = osThreadCreate (osThread (nfc_task), NULL);

    /*Enable Schedule, Start Kernel*/
#if defined(CONFIG_KERNEL) && !TASK_SCHEDULER_DISABLED
	#ifdef PLATFORM_FREERTOS
	vTaskStartScheduler();
	#endif
#else
	RtlConsolTaskRom(NULL);
#endif
}
