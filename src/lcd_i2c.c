// Credit to YWROBOT

#include "lcd_i2c.h"
#include "i2c_api.h"
#include "osdep_api.h"

uint8_t _Addr;
uint8_t _displayfunction;
uint8_t _displaycontrol;
uint8_t _displaymode;
uint8_t _numlines;
uint8_t _cols;
uint8_t _rows;
uint8_t _backlightval;

i2c_t *i2c_lcd;

/************ low level data pushing commands **********/


void expanderWrite(uint8_t _data){
	uint8_t data_send = _data | _backlightval;
	i2c_write(i2c_lcd, _Addr, &data_send, 1, 1);
}

void pulseEnable(uint8_t _data){
	expanderWrite(_data | En);	// En high
	RtlUdelayOS(1);
	expanderWrite(_data & ~((uint8_t)En));	// En low
	RtlUdelayOS(50);		// commands need > 37us to settle
}

void write4bits(uint8_t value) {
	expanderWrite(value);
	pulseEnable(value);
}

// write either command or data
void lcd_send(uint8_t value, uint8_t mode) {
	uint8_t highnib=value&0xf0;
	uint8_t lownib=(value<<4)&0xf0;
  write4bits((highnib)|mode);
	write4bits((lownib)|mode);
}

inline void lcd_write(uint8_t value) {
	lcd_send(value, Rs);
}
/*********** mid level commands, for sending data/cmds */

inline void lcd_command(uint8_t value) {
	lcd_send(value, 0);
}

/********** high level commands, for the user! */
void lcd_clear(){
	lcd_command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	RtlUdelayOS(2000);  // this command takes a long time!
}

void lcd_home(){
	lcd_command(LCD_RETURNHOME);  // set cursor position to zero
	RtlUdelayOS(2000);  // this command takes a long time!
}

void lcd_display() {
	_displaycontrol |= LCD_DISPLAYON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).
void lcd_begin(uint8_t cols, uint8_t lines, uint8_t dotsize) {
	if (lines > 1) {
		_displayfunction |= LCD_2LINE;
	}
	_numlines = lines;

	// for some 1 line displays you can select a 10 pixel high font
	if ((dotsize != 0) && (lines == 1)) {
		_displayfunction |= LCD_5x10DOTS;
	}

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
	RtlMdelayOS(50);

	// Now we pull both RS and R/W low to begin commands
	expanderWrite(_backlightval);	// reset expanderand turn backlight off (Bit 8 =1)
	RtlMdelayOS(1000);

  	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46

	  // we start in 8bit mode, try to set 4 bit mode
   write4bits(0x03 << 4);
   RtlUdelayOS(4500); // wait min 4.1ms

   // second try
   write4bits(0x03 << 4);
   RtlUdelayOS(4500); // wait min 4.1ms

   // third go!
   write4bits(0x03 << 4);
   RtlUdelayOS(150);

   // finally, set to 4-bit interface
   write4bits(0x02 << 4);

	// set # lines, font size, etc.
	lcd_command(LCD_FUNCTIONSET | _displayfunction);


	// turn the display on with no cursor or blinking default
	_displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	lcd_display();

	// clear it off
	lcd_clear();

	// Initialize to default text direction (for roman languages)
	_displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

	// set the entry mode
	lcd_command(LCD_ENTRYMODESET | _displaymode);
	lcd_home();
}

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
void init_lcd(i2c_t *i2c_obj, uint8_t lcd_Addr, uint8_t lcd_cols, uint8_t lcd_rows) {
	i2c_lcd = i2c_obj;
  _Addr = lcd_Addr;
  _cols = lcd_cols;
  _rows = lcd_rows;
  _backlightval = LCD_NOBACKLIGHT;
	_displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
	lcd_begin(_cols, _rows, LCD_5x8DOTS);
}

void lcd_setCursor(uint8_t col, uint8_t row) {
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > _numlines ) {
		row = _numlines-1;    // we count rows starting w/0
	}
	lcd_command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void lcd_noDisplay() {
	_displaycontrol &= ~LCD_DISPLAYON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}


// Turns the underline cursor on/off
void noCursor() {
	_displaycontrol &= ~LCD_CURSORON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}

void lcd_cursor() {
	_displaycontrol |= LCD_CURSORON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void lcd_noBlink() {
	_displaycontrol &= ~LCD_BLINKON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void lcd_blink() {
	_displaycontrol |= LCD_BLINKON;
	lcd_command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void lcd_scrollDisplayLeft(void) {
	lcd_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void lcd_scrollDisplayRight(void) {
	lcd_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void lcd_leftToRight(void) {
	_displaymode |= LCD_ENTRYLEFT;
	lcd_command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void lcd_rightToLeft(void) {
	_displaymode &= ~LCD_ENTRYLEFT;
	lcd_command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void lcd_autoscroll(void) {
	_displaymode |= LCD_ENTRYSHIFTINCREMENT;
	lcd_command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void lcd_noAutoscroll(void) {
	_displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	lcd_command(LCD_ENTRYMODESET | _displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void lcd_createChar(uint8_t location, uint8_t charmap[]) {
	location &= 0x7; // we only have 8 locations 0-7
	lcd_command(LCD_SETCGRAMADDR | (location << 3));
	for (int i=0; i<8; i++) {
		lcd_write(charmap[i]);
	}
}

// Turn the (optional) backlight off/on
void lcd_noBacklight(void) {
	_backlightval=LCD_NOBACKLIGHT;
	expanderWrite(0);
}

void lcd_backlight(void) {
	_backlightval=LCD_BACKLIGHT;
	expanderWrite(0);
}

// Alias functions
void lcd_cursor_on(){
	lcd_cursor();
}

void lcd_cursor_off(){
	lcd_noCursor();
}

void lcd_blink_on(){
	lcd_blink();
}

void lcd_blink_off(){
	lcd_noBlink();
}

void lcd_load_custom_character(uint8_t char_num, uint8_t *rows){
		lcd_createChar(char_num, rows);
}

void lcd_setBacklight(uint8_t new_val){
	if(new_val){
		lcd_backlight();		// turn backlight on
	}else{
		lcd_noBacklight();		// turn backlight off
	}
}

void lcd_print(const char str[]){
	int len = strlen(str);
	int idx = 0;
	while(idx < len) {
    lcd_write(str[idx]);
		idx++;
	}
}
