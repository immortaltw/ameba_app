#include "FreeRTOS.h"
#include "task.h"

#include "nfc_util.h"
#include "wifi_helper.h"

unsigned char nfc_default_uid[7] = {
    RTK_NFC_UID, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06
};

osThreadId nfc_tid = 0;
nfctag_t nfctag;
unsigned int  nfc_tag_content[NFC_MAX_PAGE_NUM];
unsigned char nfc_tag_dirty[NFC_MAX_PAGE_NUM];
flash_t flash_nfc;

extern void connect_wifi(void *param);

void nfc_event_listener(void *arg, unsigned int event) {
    switch(event) {
        case NFC_EV_READER_PRESENT:
            DiagPrintf("NFC_EV_READER_PRESENT\r\n");
            break;
        case NFC_EV_READ:
            DiagPrintf("NFC_EV_READ\r\n");
            break;
        case NFC_EV_WRITE:
            DiagPrintf("NFC_EV_WRITE\r\n");
            break;
        case NFC_EV_ERR:
            DiagPrintf("NFC_EV_ERR\r\n");
            break;
        case NFC_EV_CACHE_READ:
            DiagPrintf("NFC_EV_CACHE_READ\r\n");
            break;
    }
}

/**
 *  This callback function is called several times if tag is being written multiple pages.
 *  DO NOT put heavy task here otherwise it will block tag write and cause timeout failure.
 **/
void nfc_write_listener(void *arg, unsigned int page, unsigned int pgdat) {
    nfc_tag_content[page] = pgdat;
    nfc_tag_dirty[page] = 1;
    if (nfc_tid) {
        osSignalSet(nfc_tid, NFC_EV_WRITE);
    }
}

void nfc_read_listener(void *arg, void *buf, unsigned int page) {
    nfc_tag_content[page] = *(unsigned int *)(buf);
    nfc_tag_dirty[page] = 1;
    if (nfc_tid) {
        osSignalSet(nfc_tid, NFC_EV_READ);
    }
}

void nfc_cache_read_listener(void *arg, void *buf, unsigned int page) {
    if (nfc_tid) {
        osSignalSet(nfc_tid, NFC_EV_CACHE_READ);
    }
}

int is_valid_nfc_uid() {
    int valid_content = 1;

    unsigned char uid[7];
    unsigned char bcc[2];

    uid[0] = (unsigned char)((nfc_tag_content[0] & 0x000000FF) >>  0);
    uid[1] = (unsigned char)((nfc_tag_content[0] & 0x0000FF00) >>  8);
    uid[2] = (unsigned char)((nfc_tag_content[0] & 0x00FF0000) >> 16);
    bcc[0] = (unsigned char)((nfc_tag_content[0] & 0xFF000000) >> 24);
    uid[3] = (unsigned char)((nfc_tag_content[1] & 0x000000FF) >>  0);
    uid[4] = (unsigned char)((nfc_tag_content[1] & 0x0000FF00) >>  8);
    uid[5] = (unsigned char)((nfc_tag_content[1] & 0x00FF0000) >> 16);
    uid[6] = (unsigned char)((nfc_tag_content[1] & 0xFF000000) >> 24);
    bcc[1] = (unsigned char)((nfc_tag_content[2] & 0x000000FF) >>  0);

    // verify Block Check Character
    if (bcc[0] != (0x88 ^ uid[0] ^ uid[1] ^ uid[2])) {
        valid_content = 0;
    }
    if (bcc[1] != (uid[3] ^ uid[4] ^ uid[5] ^ uid[6])) {
        valid_content = 0;
    }

    return valid_content;
}

unsigned int generate_default_tag_content() {
    unsigned int page_size = 0;

    memset(nfc_tag_content, 0, NFC_MAX_PAGE_NUM * sizeof(unsigned int));

    // calculate Block Check Character
    unsigned char bcc[2];
    bcc[0] = 0x88 ^ nfc_default_uid[0] ^ nfc_default_uid[1] ^ nfc_default_uid[2];
    bcc[1] = nfc_default_uid[3] ^ nfc_default_uid[4] ^ nfc_default_uid[5] ^ nfc_default_uid[6];

    // generate header
    nfc_tag_content[page_size++] = ((unsigned int)nfc_default_uid[0]) <<  0 |
                                   ((unsigned int)nfc_default_uid[1]) <<  8 |
                                   ((unsigned int)nfc_default_uid[2]) << 16 |
                                   ((unsigned int)            bcc[0]) << 24;
    nfc_tag_content[page_size++] = ((unsigned int)nfc_default_uid[3]) <<  0 |
                                   ((unsigned int)nfc_default_uid[4]) <<  8 |
                                   ((unsigned int)nfc_default_uid[5]) << 16 |
                                   ((unsigned int)nfc_default_uid[6]) << 24;
    nfc_tag_content[page_size++] = ((unsigned int)            bcc[1]) <<  0;
    nfc_tag_content[page_size++] = 0x001211E1;

    // Init tag content as NDEF will-known text message "HELLO WORLD!" in little endian
    nfc_tag_content[page_size++] = 0x01d11303;
    nfc_tag_content[page_size++] = 0x6502540f;
    nfc_tag_content[page_size++] = 0x4c45486e;
    nfc_tag_content[page_size++] = 0x57204f4c;
    nfc_tag_content[page_size++] = 0x444c524f;
    nfc_tag_content[page_size++] = 0x0000fe21;

    return page_size;
}

void nfc_load_tag_content_from_flash() {
    int i, address, page_size;

    memset(nfc_tag_content, 0, NFC_MAX_PAGE_NUM * sizeof(unsigned int));
    memset(nfc_tag_dirty, 0, NFC_MAX_PAGE_NUM);

    for (i = 0, address = FLASH_APP_NFC_BASE; i < NFC_MAX_PAGE_NUM; i++, address+=4) {
        flash_read_word(&flash_nfc, address, &nfc_tag_content[i]);
    }

    if (!is_valid_nfc_uid() || NFC_RESTORE_DEFAULT) {
        DiagPrintf("Invalid tag content, restore to default value\r\n");
        page_size = generate_default_tag_content();

        // update to flash
        flash_erase_sector(&flash_nfc, FLASH_APP_NFC_BASE);
        for (i = 0, address = FLASH_APP_NFC_BASE; i < page_size; i++, address += 4) {
            flash_write_word(&flash_nfc, address, nfc_tag_content[i]);
        }
    }
}

void nfc_store_tag_content_to_flash() {
    int i, address;
    int modified_page_count;

    // dump the modified tag content
    modified_page_count = 4; // 4 for tag header
    for (i = 4; i < NFC_MAX_PAGE_NUM && nfc_tag_dirty[i]; i++) {
        modified_page_count++;
        DiagPrintf("page:%02d data:%08x\r\n", i, nfc_tag_content[i]);
    }

    flash_erase_sector(&flash_nfc, FLASH_APP_NFC_BASE);
    for (i = 0, address = FLASH_APP_NFC_BASE; i < modified_page_count; i++, address += 4) {
        flash_write_word(&flash_nfc, address, nfc_tag_content[i]);
    }
}
void nfc_parse_command_str(char *cmd) {
    // Parse first byte to get commands
}

void nfc_parse_command_raw() {
    // If data type is not text, exit right away.
    // if (((nfc_tag_content[i] >> 8) && 0xFF) != 0x54) return;

    char cmd[NFC_CMD_BUF_SIZE];
    int i, cmd_iterator = 0;
    cmd[0] = -1;

    // Get cmd string first
    // 6 for tag content start
    for (i = 6; i < NFC_MAX_PAGE_NUM && nfc_tag_dirty[i]; i++) {
        unsigned int curPage = nfc_tag_content[i];
        for (int j = 0; j < 4; ++j) {
          if (cmd[cmd_iterator] == -1) {
            cmd[cmd_iterator] = 0;
            curPage = curPage >> 8;
            continue;
          }

          if ((curPage & 0xFF) == 0xFE) break;
          cmd[cmd_iterator++] = curPage & 0xFF;
          curPage = curPage >> 8;
        }
    }
    cmd[cmd_iterator] = '\0';

    // Parse cmd
    // nfc_parse_command_str(cmd);
    if (!strncmp(cmd, "wifi", 4)) {
      // do wifi
      printf("wifi task\n");
      xTaskCreate(connect_wifi, "connect wifi", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
    } else if (!strncmp(cmd, "sensor", 6)) {
      // do sensor
      xTaskCreate(sensorTask, "sensor task", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
      xTaskCreate(clntTask, "clnt task", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
    }
}

void nfc_task(void const *arg) {
    int i;
    osEvent evt;

    nfc_load_tag_content_from_flash();

    nfc_init(&nfctag, nfc_tag_content);
    // nfc_event(&nfctag, nfc_event_listener, NULL, 0xFF);
    nfc_write(&nfctag, nfc_write_listener, NULL);
    nfc_read(&nfctag, nfc_read_listener, NULL);
    nfc_cache_read(&nfctag, nfc_cache_read_listener, NULL, 0);

    osSignalClear(nfc_tid, NFC_EV_WRITE);
    osSignalClear(nfc_tid, NFC_EV_READ);
    osSignalClear(nfc_tid, NFC_EV_CACHE_READ);

    DiagPrintf("event loop");

    while(1) {
        evt = osSignalWait (0, 0xFFFFFFFF); // wait for any signal with max timeout
        if (evt.status == osEventSignal && (evt.value.signals & NFC_EV_WRITE)) {
            osDelay(300);
            DiagPrintf("write event\r\n");
            nfc_store_tag_content_to_flash();
            nfc_parse_command_raw();

            memset(nfc_tag_dirty, 0, NFC_MAX_PAGE_NUM);
            osSignalClear(nfc_tid, NFC_EV_WRITE);
        } else if (evt.status == osEventSignal && (evt.value.signals & NFC_EV_READ)) {
          osDelay(300);
          DiagPrintf("read event\r\n");
          osSignalClear(nfc_tid, NFC_EV_READ);
        } else {
          DiagPrintf("event %x\n", evt.value.signals);
        }
    }
}
