#include "sensor_util.h"
#include "osdep_api.h"
#include "analogin_api.h"
#include <sys_api.h>
#include <inttypes.h>

// For temp/humidity sensor
uint8_t i2cdata_write[I2C_DATA_MAX_LENGTH];
uint8_t i2cdata_read[I2C_DATA_MAX_LENGTH];
uint16_t cmd;
i2c_t   *i2cmaster = NULL;
// End of temp/humidity sensor

// For MQ4/MQ7 (MQ4 to AD2, MQ7 to AD3)
analogin_t   adc1;
analogin_t   adc2;

gpio_t gpio_mq4;
gpio_t gpio_mq7;
gpio_t gpio_sensorPower;
// End of MQ4/MQ7

void initSensorPowerSwitch() {
  gpio_init(&gpio_sensorPower, D_POWER_PIN);
}

void powerUpSensors() {
  gpio_dir(&gpio_sensorPower, PIN_OUTPUT);
  gpio_mode(&gpio_sensorPower, PullUp);
  gpio_write(&gpio_sensorPower, 1);
}

void powerDownSensors() {
  gpio_dir(&gpio_sensorPower, PIN_OUTPUT);
  gpio_mode(&gpio_sensorPower, PullDown);
  gpio_write(&gpio_sensorPower, 0);
}

void TH02_IIC_WriteReg(uint8_t u8Reg, uint8_t u8Data) {
  i2cdata_write[0] = u8Reg;
  i2cdata_write[1] = u8Data;
  i2c_write(i2cmaster, TH02_I2C_DEV_ID, &i2cdata_write[0], 2, 1);
}

void TH02_IIC_WriteCmd(uint8_t u8Cmd) {
  uint8_t cmd = u8Cmd;
	i2c_write(i2cmaster, TH02_I2C_DEV_ID, &cmd, 1, 1);
}

uint8_t TH02_IIC_ReadReg(uint8_t u8Reg) {
    uint8_t Temp = 0;

	/* Send a register reading command */
  TH02_IIC_WriteCmd(u8Reg);

  i2c_read(i2cmaster, TH02_I2C_DEV_ID, (char*)&i2cdata_read[0], 1, 1);
	Temp = i2cdata_read[0];

	return Temp;
}

uint16_t TH02_IIC_ReadData2byte() {
  uint16_t TempData = 0;

	int cnt = 0;
	TH02_IIC_WriteCmd(REG_DATA_H);
  i2c_read(i2cmaster, TH02_I2C_DEV_ID, (char*)&i2cdata_read[0], 3, 1);

	/* MSB */
	TempData = (((uint16_t)i2cdata_read[1])<<8)|((uint16_t)i2cdata_read[2]);
	return TempData;
}

uint16_t TH02_IIC_ReadData(void) {
	uint16_t Temp = TH02_IIC_ReadData2byte();
	return Temp;
}


void TH02_Init(i2c_t *i2c_obj) {
  // i2c_init(&i2cmaster, MBED_I2C_MTR_SDA ,MBED_I2C_MTR_SCL);
  // i2c_frequency(&i2cmaster, MBED_I2C_BUS_CLK);
  i2cmaster = i2c_obj;
}

uint8_t TH02_isAvailable() {
    uint8_t status =  TH02_IIC_ReadReg(REG_STATUS);
	if(status & STATUS_RDY_MASK) return 0;    //ready
	else return 1;    //not ready yet
}

float TH02_ReadTemperature(void) {
    /* Start a new temperature conversion */
	TH02_IIC_WriteReg(REG_CONFIG, CMD_MEASURE_TEMP);
    //delay(100);
	/* Wait until conversion is done */
	while(!TH02_isAvailable());
	uint16_t value = TH02_IIC_ReadData();

	value = value >> 2;
	/*
	  Formula:
      Temperature(C) = (Value/32) - 50
	*/
	float temper = (((float)value)/32.0)-50.0;
	return temper;
}

float TH02_ReadHumidity(void) {
 /* Start a new humility conversion */
	TH02_IIC_WriteReg(REG_CONFIG, CMD_MEASURE_HUMI);
	/* Wait until conversion is done */
	//delay(100);
	while(!TH02_isAvailable());
	uint16_t value = TH02_IIC_ReadData();
	value = value >> 4;
	/*
	  Formula:
      Humidity(%) = (Value/16) - 24
	*/
	float humility = (value/16.0)-24.0;
	return humility;
}

/*
 *  Connect MQ4 to AD2, MQ7 to AD3
 */
void initMQSensor(int id) {
  if (id == 4) {
    analogin_init(&adc1, ADC_PIN_2);
    gpio_init(&gpio_mq4, D_MQ4_PIN);
    gpio_dir(&gpio_mq4, PIN_INPUT);
    gpio_mode(&gpio_mq4, PullNone);
  } else if (id == 7) {
    analogin_init(&adc2, ADC_PIN_3);
    gpio_init(&gpio_mq7, D_MQ7_PIN);
    gpio_dir(&gpio_mq7, PIN_INPUT);
    gpio_mode(&gpio_mq7, PullNone);
  } else printf("Wrong id\n");
}

int32_t readMQSensorAnalog(int id) {
  uint16_t rawAdc = 0, offset = OFFSET, gain = GAIN_DIV;
  int32_t mv = 0;

  if (id == 4) {
    rawAdc = analogin_read_u16(&adc1);
    printf("analog %d\n", rawAdc);
    mv = AD2MV(rawAdc, offset, gain);
  } else if (id == 7) {
    rawAdc = analogin_read_u16(&adc2);
    printf("analog %d\n", rawAdc);
    mv = AD2MV(rawAdc, offset, gain);
  }
  return mv;
}

int readMQSensorDigital(int id) {
  int ret = 0;
  if (id == 4) {
    gpio_read(&gpio_mq4);
  } else if (id == 7) {
    gpio_read(&gpio_mq7);
  }
  return ret;
}

void initAnalog(int id) {
  if (id == 1) {
    analogin_init(&adc1, ADC_PIN_2);
  } else if (id == 2) {
    analogin_init(&adc2, ADC_PIN_3);
  } else printf("Wrong id\n");
}

uint32_t readAnalog(int portNum) {
  // uint16_t rawAdc = 0, offset = OFFSET, gain = GAIN_DIV;
  uint16_t rawAdc = 0, offset = 0, gain = 1;

  int32_t mv = 0;

  if (portNum == 1) {
    rawAdc = analogin_read_u16(&adc1);
    printf("analog %d\n", rawAdc>>4);
    // mv = AD2MV(rawAdc, offset, gain);
  } else if (portNum == 2) {
    rawAdc = analogin_read_u16(&adc2);
    printf("analog %d\n", rawAdc>>4);
    // mv = AD2MV(rawAdc, offset, gain);
  }
  return (uint32_t)(rawAdc >> 4);
}
