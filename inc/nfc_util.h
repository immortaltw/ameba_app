#ifndef NFC_UTIL_H
#define NFC_UTIL_H

#include "cmsis_os.h"
#include "nfc_api.h"
#include "flash_api.h"

#define NFC_RESTORE_DEFAULT (1)
#define NFC_MAX_PAGE_NUM 36
#define RTK_NFC_UID 0x58
#define FLASH_APP_NFC_BASE   0x85000
#define NFC_CMD_BUF_SIZE 30

// NFC command types
#define NFC_CMD_SENSOR_PWR 0x01
#define NFC_CMD_WIFI_PWR   0x02
#define NFC_CMD_FETCH_DATA 0x04
#define NFC_CMD_CONFIG     0x08
#define NFC_CMD_CHECK_IN   0x10
#define NFC_CMD_RESERVED   0x20

void nfc_event_listener(void *arg, unsigned int event);
void nfc_write_listener(void *arg, unsigned int page, unsigned int pgdat);
void nfc_read_listener(void *arg, void *buf, unsigned int page);
void nfc_cache_read_listener(void *arg, void *buf, unsigned int page);
int is_valid_nfc_uid();
unsigned int generate_default_tag_content();
void nfc_load_tag_content_from_flash();
void nfc_store_tag_content_to_flash();
void nfc_parse_command_str(char *cmd);
void nfc_parse_command_raw();
void nfc_task(void const *arg);

#endif
