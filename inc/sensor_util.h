#ifndef SENSOR_UTIL_H
#define SENSOR_UTIL_H

#include "basic_types.h"
#include "i2c_api.h"
#include "PinNames.h"

#define TH02_I2C_DEV_ID      0x40
#define REG_STATUS           0x00
#define REG_DATA_H           0x01
#define REG_DATA_L           0x02
#define REG_CONFIG           0x03
#define REG_ID               0x11

#define STATUS_RDY_MASK      0x01    //poll RDY,0 indicate the conversion is done

#define CMD_MEASURE_HUMI     0x01    //perform a humility measurement
#define CMD_MEASURE_TEMP     0x11    //perform a temperature measurement

#define TH02_WR_REG_MODE      0xC0
#define TH02_RD_REG_MODE      0x80

// #define MBED_I2C_MTR_SDA    PB_3
// #define MBED_I2C_MTR_SCL    PB_2
#define MBED_I2C_MTR_SDA    PC_4
#define MBED_I2C_MTR_SCL    PC_5
#define MBED_I2C_BUS_CLK        100000  //hz
#define I2C_DATA_MAX_LENGTH     20

#define ADC_PIN_3    AD_3	// MQ7, A2
#define ADC_PIN_2    AD_2	// MQ4, A1
#define D_MQ7_PIN    PC_5 // MQ7
#define D_MQ4_PIN    PC_4 // MQ4
#define D_POWER_PIN    PA_2 // Relay
/*
 * OFFSET:   value of measuring at 0.000v, value(0.000v)
 * GAIN_DIV: value(1.000v)-value(0.000v) or value(2.000v)-value(1.000v) or value(3.000v)-value(2.000v)
 *
 * MSB 12bit of value is valid, need to truncate LSB 4bit (0xABCD -> 0xABC). OFFSET and GAIN_DIV are truncated values.
 */
#define OFFSET 		0x298
#define GAIN_DIV	0x34C
#define AD2MV(ad,offset,gain) (((ad/16)-offset)*1000/gain)

void initSensorPowerSwitch();
void powerUpSensors();
void powerDownSensors();
void TH02_IIC_WriteReg(uint8_t u8Reg, uint8_t u8Data);
void TH02_IIC_WriteCmd(uint8_t u8Cmd);
uint8_t TH02_IIC_ReadReg(uint8_t u8Reg);
uint16_t TH02_IIC_ReadData2byte();
uint16_t TH02_IIC_ReadData(void);
void TH02_Init(i2c_t *i2c_obj);
uint8_t TH02_isAvailable();
float TH02_ReadTemperature(void);
float TH02_ReadHumidity(void);
void initMQSensor(int id);
int32_t readMQSensorAnalog(int id);
int readMQSensorDigital(int id);
uint32_t readAnalog(int portNum);

#endif
