### Summary

This project is for Realtek Ameba dev board. It allows various sensors sending data to the dev board, and use the on-board wifi of the dev board to transmit the received sensor data to the cloud. Most of the sensors used in this project are connected via I2C interface, since it only requires two pins accommodate tons of sensors.

One application of this project can be found [here](http://slides.com/ianwang/acc#/). I used it to measure the readings of acc/gyro in all three axes.
